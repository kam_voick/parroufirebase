


$(document).ready(function() {
  console.log("document loaded");
  setTimeout(function(){
    $(".screenshots").css("bottom", -screenshotOffsetOf(227, 1));
  }, 500);

  appStoreBadgeWidth($(window).width());
  // appStoreBadgePos($(window).width());

  $(".screenshots img:not(#logginScreenshot)").hover(function() {
    var showScreenshotByOffset = screenshotOffsetOf(227, 1) - screenshotOffsetOf(214, 0);
    $(this).animate({bottom: showScreenshotByOffset}, 300);
  }, function() {
    $(this).animate({bottom: 0}, 300);
  });

  $("#logginScreenshot").hover(function() {
    // var showScreenshotByOffset = screenshotOffsetOf(228, 1) - screenshotOffsetOf(324, 0);
    var showScreenshotByOffset = screenshotOffsetOf(227, 1) - screenshotOffsetOf(400, 0);
    $(this).animate({bottom: showScreenshotByOffset}, 300);
  }, function() {
    $(this).animate({bottom: 0}, 300);
  });

});

function screenshotOffsetOf(p1, p2) {
  var percentageTopSpeaker = (p1 / 1776);
  var screenshotHeight = $(".screenshots").height();
  console.log("screenshotHeight" + screenshotHeight);
  var screenshotOffset = screenshotHeight * Math.abs(p2 - percentageTopSpeaker);
  return screenshotOffset;
}

function appStoreBadgeWidth(currentWidth) {
  console.log("appStoreBadgeWidth");
  var pLogoWidth = 166 / 1024;
  var width = currentWidth < 1024 ? 1024 : currentWidth;
  var calculatedBadgeMinWidth = width * pLogoWidth;
  $(".storesButtons img").css("width", calculatedBadgeMinWidth);
}

function appStoreBadgePos(currentWidth) {
  console.log("appStoreBadgePos");
  var pLogoMaxY = 324 / 1024;
  var width = currentWidth < 1024 ? 1024 : currentWidth;
  var calculatedBadgeTop = width * pLogoMaxY + 50;
  $(".storesButtons").css("top", calculatedBadgeTop);
}

$(window).resize(function(event) {
  console.log("window resized");
  // if ($(this).width() > $(this).height()){
    appStoreBadgeWidth($(this).width());
    // appStoreBadgePos($(this).width());
  // }

  $(".screenshots").css("bottom", -screenshotOffsetOf(227, 1));
});

$(window).on('load', function () {
  console.log("window loaded");

});
