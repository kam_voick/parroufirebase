module.exports = function(grunt) {

  //Configuration
  grunt.initConfig({
    concat: {
      js: {
        src: ['js/*.js'],
        dest: 'build/script.js'
      },
      css: {
        src: ['css/*.css'],
        dest: 'build/styles.css'
      }
    },
    uglify: {
      build: {
          src: 'build/script.js',
          dest: 'build/script.js'
      }
    },
    cssmin: {
    build: {
        src: 'build/styles.css',
        dest: 'build/styles.css'
  }
}

  });

  // Load plugins
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('runAll', ['concat', 'uglify', 'cssmin']);
};




// uglify: {
//     options: {
//         sourceMap: true
//     },
//     build: {
//         files: {
//             'public/all.min.js': ['public/js/vendor/jquery-1.10.2.min.js', 'public/js/*.js'],
//         }
//     }
// }
