const Vision = require('@google-cloud/vision');

module.exports = function(fileName) {
	// [START vision_safe_search_detection_gcs]
	// Imports the Google Cloud client libraries

	// Instantiates clients
	const vision = Vision();

	// The name of the bucket where the file resides, e.g. "my-bucket"
	const bucketName = 'parrou-b59e5.appspot.com';

	// The path to the file within the bucket, e.g. "path/to/image.png"
	// const fileName = 'path/to/image.png';

	const gcsPath = `gs://${bucketName}/${fileName}`;

	// Performs safe search property detection on the remote file

	return vision
		.safeSearchDetection({ source: { imageUri: gcsPath } })
		.then(results => {
			const detections = results[0].safeSearchAnnotation;

			console.log(`Adult: ${detections.adult}`);
			console.log(`Spoof: ${detections.spoof}`);
			console.log(`Medical: ${detections.medical}`);
			console.log(`Violence: ${detections.violence}`);

			var pass;
			if (
				checkNSFW(`${detections.adult}`) ||
				checkNSFW(`${detections.spoof}`) ||
				checkNSFW(`${detections.medical}`) ||
				checkNSFW(`${detections.violence}`)
			) {
				return true;
			} else {
				return false;
			}
		})
		.catch(err => {
			console.error('ERROR:', err);
			return false;
		});
	// [END vision_safe_search_detection_gcs]
};

function checkNSFW(detection) {
	var nsfw;
	if (detection == 'VERY_UNLIKELY' || detection == 'UNLIKELY') {
		nsfw = false;
	} else if (
		detection == 'POSSIBLE' ||
		detection == 'LIKELY' ||
		detection == 'VERY_LIKELY'
	) {
		nsfw = true;
	}
	return nsfw;
}
