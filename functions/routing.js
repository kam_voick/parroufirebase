const fs = require('fs');

const express = require('express');

const app = express();
app.get('/policy', (req, res) => {
	var filePath = '/docs/privacy_policy_en.pdf';

	fs.readFile(__dirname + filePath, function(err, data) {
		res.contentType('application/pdf');
		res.send(data);
	});
});
app.get('*', (req, res) => {
	res.send('/');
});

module.exports = app;
