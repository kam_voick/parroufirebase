exports.handler = function(event, admin) {
	const userID = event.params.userID;
	const newUsername = event.data.val();

	const changeup = [];

	// const changeProfilePhotoOnFollowers = admin
	// 	.database()
	// 	.ref(`/followedUsers/${userID}/followed`)
	// 	.once('value')
	// 	.then(followed => {
	// 		followed.forEach(function(childSnapshot) {
	// 			changeup[
	// 				`/followersOfUsers/${childSnapshot.key}/${userID}/username`
	// 			] = newUsername;
	// 		});
	// 		return;
	// 	});
	//
	// const changeProfilePhotoOnFollowed = admin
	// 	.database()
	// 	.ref(`/followersOfUsers/${userID}`)
	// 	.once('value')
	// 	.then(followers => {
	// 		followers.forEach(function(childSnapshot) {
	// 			changeup[
	// 				`/followedUsers/${childSnapshot.key}/followed/${userID}/username`
	// 			] = newUsername;
	// 		});
	// 		return;
	// 	});

	const changeProfilePhotoOnVotedPrivate = event.parent
		.child(`votedPolls`)
		.child(`Private`)
		.once('value')
		.then(privatePolls => {
			privatePolls.forEach(function(poll) {
				changeup[
					`/Polls/Private/${poll.val()
						.authorID}/${poll.key}/votedUsers/${poll.val().voterKey}/username`
				] = newUsername;
			});
			return;
		});

	const changeProfilePhotoOnVotedPublic = event.parent
		.child(`votedPolls`)
		.child(`Public`)
		.once('value')
		.then(publicPolls => {
			publicPolls.forEach(function(poll) {
				changeup[
					`/Polls/Public/${poll.val()
						.authorID}/${poll.key}/votedUsers/${poll.val().voterKey}/username`
				] = newUsername;
			});
			return;
		});

	return Promise.all([
		// changeUsernameOnFollowers,
		// changeUsernameOnFollowed,
		changeUsernameOnVotedPrivate,
		changeUsernameOnVotedPublic
	]).then(() => {
		admin.database().ref().update(changeup);
	});
};
