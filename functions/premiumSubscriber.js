exports.handler = function(event, admin) {
	const userID = event.params.userID;

	if (!event.data.exists() || event.data.val() == false) {
		console.log(`data doesnt exist`);
		return admin.database().ref(`subscribedUsers/${userID}`).set(null);
	} else {
		const expiredData = event.data.val();
		console.log(`data exist ${expiredData}`);

		return admin
			.database()
			.ref(`subscribedUsers/${userID}/subscribedTimestamp`)
			.set(expiredData);
	}
};
