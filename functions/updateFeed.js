const detectNSFW = require('./vision');
// const clarifai = require('./clarifai');

exports.handler = function(event, admin) {
	const userID = event.params.userID;
	const pollID = event.params.pollID;
	const poll = event.data.val();

	//poll was added
	console.log('poll was added');

	const detectLeftPhoto = detectNSFW(
		`/users_storage/${userID}/${poll.folderRefUid}/leftPhoto`
	);
	const detectRightPhoto = detectNSFW(
		`/users_storage/${userID}/${poll.folderRefUid}/rightPhoto`
	);

	return Promise.all([detectLeftPhoto, detectRightPhoto]).then(results => {
		const leftPhotoDetection = results[0];
		const rightPhotoDetection = results[1];

		if (leftPhotoDetection || rightPhotoDetection) {
			return event.data.ref.update({
				nsfw: true
			});
		}

		const updateUserTotalPublicPollsStatistics = admin
			.database()
			.ref(`/users/${userID}/statistics/polls/totalCount`)
			.transaction(totalNumber => {
				return (totalNumber || 0) + 1;
			});

		const updateUserPubliCategoryStatistics = admin
			.database()
			.ref(`/users/${userID}/statistics/polls/${poll.category}`)
			.transaction(categoryNumber => {
				return (categoryNumber || 0) + 1;
			});

		const updateFollowersTimelines = admin
			.database()
			.ref(`/followersOfUsers/${userID}/followers`)
			.once('value')
			.then(followers => {
				var promises = [];
				followers.forEach(follower => {
					let followerID = follower.key;

					var updatedPollData = {};
					updatedPollData[`/Timeline/${followerID}/${pollID}`] = {
						authorID: userID,
						timestamp: poll.timestamp
					};
					updatedPollData[
						`/CategorizedTimeline/${followerID}/${poll.category}/${pollID}`
					] = {
						authorID: userID,
						timestamp: poll.timestamp
					};
					promises.push(admin.database().ref().update(updatedPollData));
				});
				return Promise.all(promises);
			});

		return Promise.all([
			updateUserTotalPublicPollsStatistics,
			updateUserPubliCategoryStatistics,
			updateFollowersTimelines
		]);
	});
};
