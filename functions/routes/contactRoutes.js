const Mailer = require('../services/Mailer');

module.exports = app => {
	app.post('/contact', (req, res) => {
		console.log(req.body);

		const mailer = new Mailer(req.body);
		return mailer
			.send()
			.then(() => {
				console.log('New contact message from:', req.body.email);
				res.sendStatus(200);
			})
			.catch(error => {
				console.error('There was an error while sending the email:', error);
				res.status(422).send(error);
			});
	});
};
