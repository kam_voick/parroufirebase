exports.cleanupExpiredSubscribers = function(req, res, admin) {
	const currentTime = new Date().getTime();
	const lastMonth = currentTime - 2592000000;

	return admin
		.database()
		.ref(`subscribedUsers`)
		.orderByChild('subscribedTimestamp')
		.endAt(lastMonth)
		.once('value')
		.then(subscribers => {
			if (!subscribers.exists()) {
				console.log('There are no quered subscribers');
				res.send('There are no quered subscribers');
				return;
			}
			const subscribersIDs = Object.keys(subscribers.val());
			const cleanupPaths = {};
			subscribersIDs.forEach(id => {
				cleanupPaths[`subscribedUsers/${id}`] = null;
			});
			console.log(
				`cleanedUp ${subscribersIDs.length} expired subscribers ${subscribersIDs}`
			);

			return admin
				.database()
				.ref()
				.update(cleanupPaths)
				.then(() => {
					res.send(
						`cleanedUp ${cleanupPaths.length} expired subscribers ${cleanupPaths}`
					);
					return;
				})
				.catch(error => {
					res.send(error);
				});
		});
};
