const functions = require('firebase-functions');
const sendgrid = require('sendgrid');
const helper = sendgrid.mail;

class Mailer extends helper.Mail {
	constructor({ email: userEmail, subject, message }) {
		super();

		this.sgApi = sendgrid(functions.config().sendgrid.key);
		this.from_email = new helper.Email(userEmail);
		this.subject = subject;
		this.body = new helper.Content('text/plain', message);

		this.addContent(this.body);
		this.addRecipient();
	}

	formatAddresses(recipients) {
		return recipients.map(({ email }) => {
			return new helper.Email(email);
		});
	}

	addRecipient() {
		const personalize = new helper.Personalization();
		const toEmail = new helper.Email('voick.parrou@gmail.com');
		personalize.addTo(toEmail);
		this.addPersonalization(personalize);
	}

	send() {
		const request = this.sgApi.emptyRequest({
			method: 'POST',
			path: '/v3/mail/send',
			body: this.toJSON()
		});

		return this.sgApi.API(request);
	}
}

module.exports = Mailer;
