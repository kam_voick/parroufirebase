'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');

let updateFeed = require('./updateFeed');

let addFollower = require('./addFollower');
let removeFollower = require('./removeFollower');

let usernameChanged = require('./usernameChanged');
let profilePhotoChanged = require('./profilePhotoChanged');
let newFacebookFriend = require('./newFacebookFriend');
let removeUser = require('./removeUser');
let premiumSubscriber = require('./premiumSubscriber');

admin.initializeApp(functions.config().firebase);

exports.updateFeed = functions.database
	.ref('/Polls/Public/{userID}/{pollID}')
	.onCreate(event => {
		updateFeed.handler(event, admin);
	});

exports.addFollower = functions.database
	.ref('/followersOfUsers/{followedID}/followers/{followerID}')
	.onCreate(event => {
		addFollower.handler(event, admin);
	});
exports.removeFollower = functions.database
	.ref('/followersOfUsers/{followedID}/followers/{followerID}')
	.onDelete(event => {
		removeFollower.handler(event, admin);
	});

exports.usernameChanged = functions.database
	.ref('/users/{userID}/username')
	.onUpdate(event => {
		usernameChanged.handler(event, admin);
	});

exports.profilePhotoChanged = functions.database
	.ref('/users/{userID}/username')
	.onUpdate(event => {
		profilePhotoChanged.handler(event, admin);
	});

exports.removeUser = functions.auth.user().onDelete(event => {
	removeUser.handler(event, admin);
});
exports.newFacebookFriend = functions.database
	.ref('/users/{userID}/facebookFriends')
	.onCreate(event => {
		newFacebookFriend.handler(event, admin);
	});

exports.premiumSubscriber = functions.database
	.ref('/users/{userID}/subscriptionExpiryDate')
	.onWrite(event => {
		premiumSubscriber.handler(event, admin);
	});

const routes = require('./routes');
exports.api = functions.https.onRequest(routes);

const cronJobs = require('./cronJobs');
exports.cleanupExpiredSubscribers = functions.https.onRequest((req, res) => {
	cronJobs.cleanupExpiredSubscribers(req, res, admin);
});
