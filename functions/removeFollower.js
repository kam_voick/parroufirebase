exports.handler = function(event, admin) {
	const followedID = event.params.followedID;
	const followerID = event.params.followerID;

	console.log('User ', followerID, ' un-followed user ', followedID);

	const deletingPollsByUnfollowed = admin
		.database()
		.ref('/Polls/Public/' + followedID)
		.once('value')
		.then(polls => {
			if (!polls.hasChildren()) {
				return console.log(
					'There are no polls which can be deleted by ' + followedID
				);
			}

			const pollsToDelete = [];
			polls.forEach(poll => {
				var updatedPollData = {};
				updatedPollData[`/Timeline/${followerID}/${poll.key}`] = null;
				updatedPollData[
					`/CategorizedTimeline/${followerID}/${poll.val()
						.category}/${poll.key}`
				] = null;
				pollsToDelete.push(admin.database().ref().update(updatedPollData));
			});
			console.log(
				'removed followed content from timeline of user: ' + followerID
			);
			return Promise.all(pollsToDelete);
		});

	const decreaseFollowedNumber = admin
		.database()
		.ref(`users/${followerID}/followedNumber`)
		.transaction(number => {
			if (!event.data.exists() && event.data.previous.exists()) {
				return (number || 0) - 1;
			}
		})
		.then(() => {
			return console.log('Decreased users followed count');
		});

	const decreaseFollowersNumber = admin
		.database()
		.ref(`users/${followedID}/followersNumber`)
		.transaction(number => {
			if (!event.data.exists() && event.data.previous.exists()) {
				return (number || 0) - 1;
			}
		})
		.then(() => {
			return console.log('Decreased users followers count');
		});

	return Promise.all([
		deletingPollsByUnfollowed,
		decreaseFollowedNumber,
		decreaseFollowersNumber
	]);
};
