const _ = require('lodash');

exports.handler = function(event, admin) {
	const userID = event.data.uid;

	const cleanup = {};

	cleanup[`/users/${userID}`] = null;
	cleanup[`/followedUsers/${userID}`] = null;
	cleanup[`/Timeline/${userID}`] = null;
	cleanup[`/ReportedUsers/${userID}`] = null;
	cleanup[`/Polls/Public/${userID}`] = null;
	cleanup[`/Polls/Private/${userID}`] = null;
	cleanup[`/CategorizedTimeline/${userID}`] = null;

	const removeFromPrvateVotedPolls = admin
		.database()
		.ref(`/users/${userID}/votedPolls/Private`)
		.once('value')
		.then(votedPolls => {
			if (!votedPolls.hasChildren()) {
				return;
			}
			const votedPollsIDs = _.keys(votedPolls.val());
			votedPollsIDs.forEach(poll => {
				cleanup[`/Polls/Private/${poll.authorID}/${poll.key}`] = null;
			});
			return;
		});

	const removeFromPublicVotedPolls = admin
		.database()
		.ref(`/users/${userID}/votedPolls/Public`)
		.once('value')
		.then(votedPolls => {
			if (!votedPolls.hasChildren()) {
				return;
			}
			const votedPollsIDs = _.keys(votedPolls.val());
			votedPollsIDs.forEach(poll => {
				cleanup[`/Polls/Public/${poll.authorID}/${poll.key}`] = null;
			});
			return;
		});

	const removeFromFollowers = admin
		.database()
		.ref(`followersOfUsers/${userID}/followers`)
		.once('value')
		.then(followers => {
			if (!followers.hasChildren()) {
				return;
			}

			cleanup[`/accountsToDelete/${userID}`] = true;

			const followersIDs = _.keys(followers.val());

			console.log(`followersIDs ${followersIDs}`);

			const promises = [];
			followersIDs.forEach(followerID => {
				cleanup[`/followedUsers/${followerID}/followed/${userID}`] = null;

				const decreaseFollowedNumber = admin
					.database()
					.ref(`users/${followerID}/followedNumber`)
					.transaction(number => {
						return (number || 0) - 1;
					})
					.then(() => {
						return console.log('Decreased users followers number');
					});
				promises.push(decreaseFollowedNumber);
			});

			console.log(cleanup);

			return Promise.all(promises);
		});

	return Promise.all([
		removeFromPrvateVotedPolls,
		removeFromPublicVotedPolls,
		removeFromFollowers
	])
		.then(() => {
			console.log('cleanup');
			return admin.database().ref().update(cleanup);
		})
		.catch(reason => {
			console.error(reason);
		});
};
