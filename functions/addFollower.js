exports.handler = function(event, admin) {
	const followedID = event.params.followedID;
	const followerID = event.params.followerID;
	// const poll = event.data.val()

	console.log('User: ', followedID, 'have new follower: ', followerID);

	const getDeviceTokensPromise = admin
		.database()
		.ref('/users/' + followedID + '/notificationTokens')
		.once('value');

	const getFollowerProfilePromise = admin.auth().getUser(followerID);

	const sendingFCM = Promise.all([
		getDeviceTokensPromise,
		getFollowerProfilePromise
	]).then(results => {
		const tokensSnapshot = results[0];
		const follower = results[1];

		//check if there are any device tokens
		if (!tokensSnapshot.hasChildren()) {
			return console.log('There are no notification tokens to send to');
		}

		console.log(
			'There are',
			tokensSnapshot.numChildren(),
			'tokens to send notification to.'
		);

		var arr = [`${follower.displayName}`];
		var followerJSON = JSON.stringify(arr);

		const payload = {
			notification: {
				titleLocKey: 'NEW_FOLLOWER_TITLE',
				bodyLocKey: 'NEW_FOLLOWER_BODY',
				bodyLocArgs: followerJSON
			}
		};

		//listing all tokens
		const tokens = Object.keys(tokensSnapshot.val());

		//Send notification to all tokens
		return admin.messaging().sendToDevice(tokens, payload).then(response => {
			//for each message check if there was an error
			const tokensToRemove = [];
			response.results.forEach((result, index) => {
				const error = result.error;
				if (error) {
					console.error(
						'Failure sending notification to ',
						tokens[index],
						error
					);
					//cleanup the tokens who are not registered anymore
					if (
						error.code === 'messaging/invalid-registration-token' ||
						error.code === 'messaging/registration-token-not-registered'
					) {
						tokensToRemove.push(
							tokensSnapshot.ref.child(tokens[index]).remove()
						);
					}
				}
			});
			return Promise.all(tokensToRemove);
		});
	});

	const fetchingFollowedPollsByFollower = admin
		.database()
		.ref(`/Polls/Public/${followedID}`)
		.once('value')
		.then(snap => {
			if (!snap.hasChildren()) {
				return console.log(
					'There are no polls which can be delivered by ' + followedID
				);
			}

			const pollsToDeliver = [];
			snap.forEach(poll => {
				var updatedPollData = {};
				updatedPollData[`/Timeline/${followerID}/${poll.key}`] = {
					authorID: followedID,
					timestamp: poll.val().timestamp
				};
				updatedPollData[
					`/CategorizedTimeline/${followerID}/${poll.val()
						.category}/${poll.key}`
				] = {
					authorID: followedID,
					timestamp: poll.val().timestamp
				};
				pollsToDeliver.push(admin.database().ref().update(updatedPollData));
			});
			console.log('added previous polls to timeline of user: ' + followerID);
			return Promise.all(pollsToDeliver);
		});

	const increaseFollowedNumber = admin
		.database()
		.ref(`users/${followerID}/followedNumber`)
		.transaction(number => {
			if (event.data.exists() && !event.data.previous.exists()) {
				return (number || 0) + 1;
			}
		})
		.then(() => {
			return console.log('Increased users followed count');
		});

	const increaseFollowersNumber = admin
		.database()
		.ref(`users/${followedID}/followersNumber`)
		.transaction(number => {
			if (event.data.exists() && !event.data.previous.exists()) {
				return (number || 0) + 1;
			}
		})
		.then(() => {
			return console.log('Increased users followers count');
		});

	return Promise.all([
		sendingFCM,
		fetchingFollowedPollsByFollower,
		increaseFollowedNumber,
		increaseFollowersNumber
	]);
};
