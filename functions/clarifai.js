var SFW_LOWER_LIMIT = 0.8;

module.exports = function(fileName) {
	// Require the client
	const Clarifai = require('clarifai');

	// initialize with your api key. This will also work in your browser via http://browserify.org/
	const app = new Clarifai.App({
		apiKey: '{e8b0a9b2d6d649668d679336c5b81000}'
	});

	console.log('fileName' + fileName);

	app.models.predict('d16f390eb32cad478c7ae150069bd2c6', fileName).then(
		function(response) {
			// do something with response
			var pass;
			let data = JSON.parse(response.request.response);
			let concepts = data.outputs[0].data.concepts;
			concepts.forEach(concept => {
				console.log(`concept ${concept}`);
				if (concept.name == 'sfw') {
					pass = v.value > SFW_LOWER_LIMIT;
				}
			});
			console.log(`did it pass ? ${pass}`);
		},
		function(err) {
			// there was an error
			console.log(`error ${err}`);
		}
	);
};
