exports.handler = function(event, admin) {
	const userID = event.params.userID;

	return admin.auth().getUser(userID).then(newUser => {
		const newUserFacebookName = newUser.providerData.displayName;

		var promises = [];
		event.data.forEach(function(facebookFriend) {
			const facebookFriendID = facebookFriend.key;
			const existedFacebookFriendName = facebookFriend.val();

			const sendNotificationToFacebookFriend = admin
				.database()
				.ref()
				.child('users')
				.orderByChild('facebookID')
				.equalTo(`${facebookFriendID}`)
				.once('value')
				.then(
					facebookFriendData => {
						if (!facebookFriendData.exists()) {
							return console.log(
								`facebook friend ${existedFacebookFriendName} doesnt exist `
							);
						}

						const facebookFriendVal = facebookFriendData.val();
						console.log(`existed fb friend ${existedFacebookFriendName}`);

						const tokensData =
							facebookFriendVal[Object.keys(facebookFriendVal)[0]]
								.notificationTokens;
						console.log(tokensData);

						//listing all tokens
						const tokens = Object.keys(tokensData);
						console.log(tokens);

						//check if there are any device tokens
						if (tokens.length == 0) {
							return console.log('There are no notification tokens to send to');
						} else {
							console.log(`There are ${tokens.length} notifications to send`);
						}

						var arr = [`${newUserFacebookName}`];
						var newFacebookFriendJSON = JSON.stringify(arr);

						const payload = {
							notification: {
								bodyLocKey: 'NEW_FACEBOOK_FRIEND_BODY',
								bodyLocArgs: newFacebookFriendJSON
							}
						};

						//Send notification to all tokens
						return admin
							.messaging()
							.sendToDevice(tokens, payload)
							.then(response => {
								//for each message check if there was an error
								const tokensToRemove = [];
								response.results.forEach((result, index) => {
									const error = result.error;
									if (error) {
										console.error(
											'Failure sending notification to ',
											tokens[index],
											error
										);
										//cleanup the tokens who are not registered anymore
										if (
											error.code === 'messaging/invalid-registration-token' ||
											error.code ===
												'messaging/registration-token-not-registered'
										) {
											tokensToRemove.push(
												tokensSnapshot.ref.child(tokens[index]).remove()
											);
										}
									}
								});
								return Promise.all(tokensToRemove);
							});
					},
					function(error) {
						console.error(error);
					}
				);
			promises.push(sendNotificationToFacebookFriend);
		});
		return Promise.all(promises);
	});
};
