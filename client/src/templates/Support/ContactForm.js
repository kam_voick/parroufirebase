import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';

import ContactField from './ContactField';

class ContactForm extends Component {
	render() {
		return (
			<form
				ref={this.props.formRef}
				className="text-center"
				action=""
				onSubmit={this.props.handleSubmit(this.props.onSubmit)}>
				<label
					className="w-100 pb-3"
					style={{
						font: 'normal 1.5rem "Futura", Helvetica, sans-serif'
					}}>
					Send us message!
				</label>
				<div className="form-group row align-items-top">
					<label
						htmlFor="inputEmail3"
					className="col-sm-4 col-md-3 col-form-label text-left">
						Email:
					</label>
					<div className="col-sm-8 col-md-9">
						<Field
							id="inputEmail3"
							className="form-control"
							type="email"
							name="email"
							placeholder="email@example.com"
							component={ContactField}
						/>
					</div>
				</div>
				<div className="form-group row align-items-top">
					<label
						htmlFor="selectSubject"
					className="col-sm-4 col-md-3 col-form-label text-left">
						Subject:
					</label>
					<div className="col-sm-8 col-md-9">
						<Field
							className="form-control"
							type="text"
							name="subject"
							component={ContactField}
						/>
					</div>
				</div>
				<div className="form-group">
					<label htmlFor="messageInput">Message</label>
					<Field
						className="form-control"
						type="text"
						name="message"
						cols="30"
						rows="10"
						component="textarea"
						style={{ resize: 'none' }}
					/>
				</div>
				<button className="btn btn-primary" type="submit">
					Submit
				</button>
			</form>
		);
	}
}

function validate(values) {
	const errors = {};

	if (!values.email) {
		errors.email = 'You must provide an email';
	} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
		errors.email = 'Invalid email address';
	}
	if (!values.subject) {
		errors.subject = 'You must provide a subject';
	}

	return errors;
}

export default reduxForm({ validate, form: 'contactForm' })(ContactForm);
