import React from 'react';

export default ({
	placeholder,
	input,
	label,
	meta: { touched, error, warning }
}) => {
	if (touched && error) {
		return (
			<div className="form-group has-warning mb-0">
				<input
					className="form-control form-control-warning"
					id="inputWarning1"
					{...input}
				/>
				<small className="form-control-feedback">{error}</small>
			</div>
		);
	} else {
		return (
			<div>
				<input className="form-control" {...input} placeholder={placeholder} />
			</div>
		);
	}
};
