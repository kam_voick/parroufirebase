import React from 'react';
import { Transition } from 'react-transition-group';

const MessageSent = ({ height }) => {
	return (
		<div
			className="messageSent d-flex justify-content-center align-items-center"
			style={{
				height: `${height}px`,
				backgroundColor: `rgba(124, 153, 180, 0.1)`,
				borderRadius: '5px'
			}}>
			<div
				style={{ fontFamily: 'Futura, Helvetica, sans-serif' }}
				className="text-center px-2">
				<h4>Thanks for sending message</h4>
				<h5>We'll try to anwear you as soon as posible</h5>
			</div>
		</div>
	);
};

export default MessageSent;
