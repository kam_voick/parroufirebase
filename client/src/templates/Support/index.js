import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/index';
import { findDOMNode } from 'react-dom';

import './style.css';

import ContactForm from './ContactForm';
import MessageSent from './MessageSent';

class Support extends Component {
	state = { messageSent: false };
	submit = values => {
		console.log(values);
		this.props.submitMessage(values);
		this.setState({ messageSent: true });
	};

	componentDidMount() {
		this.cardHeight = findDOMNode(this.formRef).clientHeight;
		console.log(this.cardHeight);
	}

	renderContent() {
		if (this.state.messageSent) {
			return <MessageSent height={this.cardHeight} />;
		}
		return (
			<ContactForm
				formRef={ref => (this.formRef = ref)}
				onSubmit={this.submit}
			/>
		);
	}

	render() {
		return (
			<div className="container">
				<div className="supportContainer w-100 row no-gutters align-items-center my-auto">
					<div className="col-sm-6 col-md-7 my-3 h-100 align-items-center text-center">
						<h3 style={styles}>HAVE ANY QUESTIONS?</h3>
						<h1 style={styles}>Contact us.</h1>
					</div>
					<div className="col-sm-6 col-md-5 p-3">{this.renderContent()}</div>
				</div>
			</div>
		);
	}
}

const styles = {
	color: 'rgb(78, 72, 72)',
	fontFamily: 'Futura, Helvetica, sans-serif'
};

export default connect(null, actions)(Support);
