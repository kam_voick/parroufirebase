import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { CSSTransition } from 'react-transition-group';
import mammoth from 'mammoth';

import Home from './Home';
import Dashboard from './Dashboard';
import Header from './Header';
import Footer from './Footer';
import Support from './Support';
import Terms from './Terms';

import './app.css';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = { termsConverted: null, fadeInEntered: false };

		this.convertDocxToHtml = this.convertDocxToHtml.bind(this);
		this.renderTerms = this.renderTerms.bind(this);
	}
	componentDidMount() {
		this.convertDocxToHtml();
	}

	async convertDocxToHtml() {
		const resp = await fetch('/docs/terms_and_conditions_en.docx');
		const arrayBuffer = await resp.arrayBuffer();
		function transformElement(element) {
			if (element.styleId === 'CustomHeader') {
				console.log(element);
				return { ...element, alignment: 'center' };
			} else {
				return element;
			}
		}
		var options = {
			styleMap: [
				"p[style-name='CustomTitle'] => h1:fresh",
				"p[style-name='customAnnotation'] => h5:fresh",
				"p[style-name='CustomHeader'] => h3:fresh"
			]
		};
		const result = await mammoth.convertToHtml(
			{
				arrayBuffer: arrayBuffer
			},
			options
		);
		console.log(result);
		this.setState({ termsConverted: result.value });
	}

	renderTerms() {
		if (!this.state.termsConverted) {
			return <div />;
		}
		return <Terms termsHTML={this.state.termsConverted} />;
	}

	render() {
		console.log('rendered app');
		return (
			<div style={{ background: '#5a5353' }}>
				<BrowserRouter>
					<div style={{ minHeight: '100vh' }}>
						<Route exact path="/" component={Home} />
						<Route
							path="/home"
							render={({ history, location }) => (
								<div>
									<div style={{ minHeight: '100vh' }}>
										<Header location={location} history={history} />
										<CSSTransition
											key={location.key}
											in={true}
											appear
											classNames="fade"
											timeout={1000}>
											<Switch location={location}>
												<Route exact path="/home" component={Dashboard} />
												<Route path="/home/support" component={Support} />
												<Route path="/home/terms" render={this.renderTerms} />
											</Switch>
										</CSSTransition>
									</div>
									<Footer />
								</div>
							)}
						/>
					</div>
				</BrowserRouter>
			</div>
		);
	}
}

export default App;
