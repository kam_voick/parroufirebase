import React, { Component } from 'react';
import Carousel from '../../components/carousel';
import IndicatorLines from '../../components/carousel/indicator-lines';
import Buttons from '../../components/carousel/buttons';

import firstPhoto from './first.png';
import secondPhoto from './first.png';
import thirdPhoto from './first.png';

export default class Dashboard extends Component {
	renderSlides() {
		return (
			<div
				style={{
					margin: 'auto',
					height: '50%',
					width: '90%',
					backgroundColor: '#d6cfcb',
					borderRadius: '5px',
					filter: 'drop-shadow(0 2px 2px rgba(0, 0, 0, 0.3))',
					position: 'relative'
				}}
			/>
		);
	}

	render() {
		return (
			<div className="container" style={{ height: '75vh', display: 'grid' }}>
				<Carousel loop auto widgets={[IndicatorLines]}>
					<div style={{ height: '100%' }}>{this.renderSlides()}</div>
					<div style={{ height: '100%' }}>{this.renderSlides()}</div>
					<div style={{ height: '100%' }}>{this.renderSlides()}</div>
					<div style={{ height: '100%' }}>{this.renderSlides()}</div>
				</Carousel>
			</div>
		);
	}
}

const styles = {
	color: 'rgb(78, 72, 72)',
	fontFamily: 'Futura, Helvetica, sans-serif'
};
