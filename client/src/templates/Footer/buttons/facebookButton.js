import React from 'react';

const FacebookButton = props => (
	<svg height="100%" viewBox="0 0 183 183" {...props}>
		<title>facebookButton</title>
		<path
			d="M91.5 183C40.966 183 0 142.034 0 91.5S40.966 0 91.5 0 183 40.966 183 91.5 142.034 183 91.5 183zm7.85-38V95.736h16.51l2.472-19.2H99.349V64.28c0-5.56 1.541-9.347 9.5-9.347L119 54.929V37.757c-1.756-.234-7.781-.757-14.792-.757-14.635 0-24.655 8.947-24.655 25.378v14.159H63v19.199h16.553V145h19.796z"
			fill="#d6cfcb"
			fillRule="evenodd"
		/>
	</svg>
);

export default FacebookButton;
