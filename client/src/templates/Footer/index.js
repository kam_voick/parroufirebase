import React, { Component } from 'react';
import './style.css';

import EmailButton from './buttons/emailButton.js';
import FacebookButton from './buttons/facebookButton.js';

export default class Footer extends Component {
	render() {
		return (
			<div className="container-fluid mt-5" style={{ opacity: '0.5' }}>
				{/* <div className="row mx-5 mt-5 mb-2">
					<hr
						className="col"
						style={{
					backgroundColor: '#d6cfcb',
					border: '1px solid #d6cfcb',
					borderRadius: '3px',
					margin: 'auto'
						}}
					/>
					<svg
						className="mx-3"
						style={{ width: '20px', height: '80px' }}
						viewBox="0 0 205 275"
						preserveAspectRatio="xMidYMid meet">
						<path
					d="M68.77 224.762v39.283c0 5.667 10.202 10.955 18.893 10.955 5.668 0 10.58-2.266 10.58-8.688v-29.606c0-26.604 9.986-23.407 29.714-29.683 43.902-13.966 76.841-61.136 76.841-114.853 0-53.64-35.896-92.17-91.063-92.17C46.476 0 0 46.085 0 116.724c0 44.196 21.538 66.861 27.584 72.527 10.957 8.31 21.16-16.62 21.16-23.42 0-3.777-1.512-6.422-5.668-5.288-.756-1.134-14.359-12.844-14.359-46.086 0-46.085 24.56-86.126 85.396-86.126 44.21 0 61.59 35.886 61.59 64.595 0 63.461-51.766 92.17-77.46 95.192-20.076 2.361-29.473 18.905-29.473 36.644z"
					fill="#d6cfcb"
					fillRule="evenodd"
						/>
					</svg>
					<hr
						className="col"
						style={{
					backgroundColor: '#d6cfcb',
					border: '1px solid #d6cfcb',
					borderRadius: '3px',
					margin: 'auto'
						}}
					/>
				</div> */}
				<div
					className="row w-100 no-gutters justify-content-center"
					style={{ height: '35px' }}>
					<a href="">
						<FacebookButton height="100%" className="mx-3" />
					</a>
					<a href="mailto:hello@parrou.pl">
						<EmailButton height="100%" className="mx-3" />
					</a>
				</div>
				<div
					className="w-100 d-flex justify-content-center"
					style={{
						color: '#d6cfcb',
						font: 'normal 1rem "Futura", Helvetica, sans-serif'
					}}>
					<div className="mt-4 mb-3">© Parrou 2017</div>
				</div>
			</div>
		);
	}
}
