import React, { Component } from 'react';
import { Transition } from 'react-transition-group';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as actions from '../../actions';

import './style.css';

import Screenshots from '../../components/screenshots';
import StoreBadges from '../../components/storeBadges';

const duration = 1000;

const defaultStyle = {
	transition: `height ${duration}ms ease-in-out, transform ${duration}ms ease-in-out, filter ${duration}ms ease-in-out`,
	overflow: 'hidden',
	filter: `drop-shadow(0 16px 12px rgba(0, 0, 0, 0.5))`,
	height: `100vh`,
	transform: `translateY(0vh)`
};

class Home extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loggedInRedirect: false,
			loggedOutRedirect: false
		};
	}

	render() {
		if (this.state.loggedInRedirect) {
			return <Redirect to="/home" />;
		}

		const transitionStyles = {
			entering: {
				filter: `drop-shadow(0 16px 12px rgba(0, 0, 0, ${0}))`,
				height: `${this.props.loggedIn === false ? 100 : 0}vh`,
				transform: `translateY(-100vh)`
			},
			entered: {
				filter: `drop-shadow(0 16px 12px rgba(0, 0, 0, ${this.props.loggedIn ===
				false
					? 0.5
					: 0}))`,
				height: `${this.props.loggedIn === false ? 100 : 0}vh`,
				transform: `translateY(${this.props.loggedIn === false ? 0 : -100}vh)`
			}
		};

		return (
			<Transition
				in={this.props.loggedIn !== null}
				appear={this.props.loggedIn === false}
				onEntered={() => {
					if (this.props.loggedIn) {
						this.setState({ loggedInRedirect: true });
					}
				}}
				timeout={this.props.loggedIn ? duration : 0}>
				{state => (
					<div
						className="container p-0"
						style={{
							...defaultStyle,
							...transitionStyles[state]
						}}>
						<div className="container home">
							<StoreBadges />
							<Screenshots />
						</div>
					</div>
				)}
			</Transition>
		);
	}
}

function mapStateToProps({ loggedIn }) {
	return { loggedIn };
}

export default connect(mapStateToProps, actions)(Home);
