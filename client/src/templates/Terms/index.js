import React, { Component } from 'react';

export default class Terms extends Component {
	render() {
		return (
			<div
				onTransitionEnd={() => {
					console.log('transitionend');
				}}
			className="container">
				<div className="supportContainer">
					<div className="px-5 py-5">
						<div dangerouslySetInnerHTML={{ __html: this.props.termsHTML }} />
					</div>
				</div>
			</div>
		);
	}
}
