import React, { Component, PureComponent } from 'react';
import { findDOMNode } from 'react-dom';
import { connect } from 'react-redux';
import { NavLink, Redirect } from 'react-router-dom';
import * as actions from '../../actions';

import './style.css';

import logo from './images/MainLogo2.svg';

class Header extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			underlineX: -500,
			underlineWidth: 0
		};

		this.moveUnderlineToPos = this.moveUnderlineToPos.bind(this);
		this.setCurrentLinkBy = this.setCurrentLinkBy.bind(this);

		window.addEventListener('resize', () => this.moveUnderlineToPos());
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.location !== this.props.location) {
			this.setCurrentLinkBy(nextProps.location);
		}
	}

	shouldComponentUpdate(nextProps, nextState) {
		if (
			nextState.underlineX === this.state.underlineX &&
			nextState.underlineWidth === this.state.underlineWidth
		) {
			return false;
		}
		return true;
	}

	setCurrentLinkBy(location) {
		var lastRef;
		switch (location.pathname) {
			case '/home':
				lastRef = this.homeRef;
				break;
			case '/home/terms':
				lastRef = this.termsRef;
				break;
			case '/home/support':
				lastRef = this.contactRef;
				break;
			default:
				lastRef = this.homeRef;
				break;
		}
		this.moveUnderlineToPos(lastRef);
		this.lastRef = lastRef;
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.calculateBottomOffset);
	}

	moveUnderlineToPos(target = this.lastRef) {
		const { offsetLeft, clientWidth } = findDOMNode(target);

		const underlineWidth = clientWidth * 0.71;
		const underlineX = offsetLeft + clientWidth / 2 - underlineWidth / 2;

		this.setState({ underlineX, underlineWidth });
	}

	render() {
		if (this.state.loggedOutRedirect) {
			return <Redirect to="/" />;
		}
		return (
			<div className="row no-gutters" style={{ height: '25vh' }}>
				<nav
					className="w-100 nav p-0 align-items-end justify-content-sm-between justify-content-center"
					style={{
						font: 'normal 1rem "Futura", Helvetica, sans-serif'
					}}>
					<NavLink exact to="/home">
						<img
							ref={ref => {
								this.homeRef = ref;
							}}
							// onClick={() => {
							// 	this.props.loggedInSuccessfully(false);
							// 	this.setState({ loggedOutRedirect: true });
							// }}
							onMouseOver={event => this.moveUnderlineToPos(event.target)}
							onMouseOut={event => this.moveUnderlineToPos()}
							src={logo}
							alt=""
							onLoad={() => this.setCurrentLinkBy(this.props.location)}
							className="img-fluid nav-item px-4 py-3 navbar-brand"
							style={{
								height: '80px',
								filter: 'drop-shadow(0 2px 2px rgba(0, 0, 0, 0.2))'
							}}
						/>
					</NavLink>

					<div className="navbar-nav navbar-expand">
						<NavLink
							innerRef={ref => {
								this.faqRef = ref;
							}}
							className="nav-item nav-link py-3 px-4"
							exact
							to="/home/faq"
							onMouseOver={event => this.moveUnderlineToPos(event.target)}
							onMouseOut={event => this.moveUnderlineToPos()}>
							FAQ
						</NavLink>
						<NavLink
							innerRef={ref => {
								this.termsRef = ref;
							}}
							className="nav-item nav-link py-3 px-4"
							exact
							to="/home/terms"
							onMouseOver={event => this.moveUnderlineToPos(event.target)}
							onMouseOut={event => this.moveUnderlineToPos()}>
							TERMS & PRIVACY
						</NavLink>
						<NavLink
							innerRef={ref => {
								this.contactRef = ref;
							}}
							className="nav-item nav-link py-3 px-4"
							onMouseOver={event => this.moveUnderlineToPos(event.target)}
							onMouseOut={event => this.moveUnderlineToPos()}
							exact
							to="/home/support">
							CONTACT
						</NavLink>
					</div>
				</nav>

				<hr
					style={{
						width: `${this.state.underlineWidth}px`,
						border: '2px solid #d6cfcb',
						borderRadius: '3px',
						margin: '0',
						left: `${this.state.underlineX - 1}px`,
						position: 'relative',
						transition: `left 0.3s ease-in-out 0s, width 0.3s ease-in-out 0s`
					}}
				/>
			</div>
		);
	}
}

export default connect(null, actions)(Header);
