import { FETCH_USER } from './types';
import axios from 'axios';

export const loggedInSuccessfully = bool => dispatch => {
	console.log(`loggedin: ${bool}`);
	dispatch({ type: FETCH_USER, payload: bool });
};

export const submitMessage = values => async dispatch => {
	const res = await axios.post(
		`${process.env.REACT_APP_SERVER_URL}/api/contact`,
		values
	);
	return { type: 'message_submit' };
};
