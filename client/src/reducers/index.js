import { combineReducers } from 'redux';
import loggedInReducer from './reducer_loggedIn';
import { reducer as reduxForm } from 'redux-form';

export default combineReducers({
	loggedIn: loggedInReducer,
	form: reduxForm
});

//logged just for now
