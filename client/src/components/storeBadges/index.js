import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../../actions';

import appStoreBadge from './images/overlayedAppStoreButton.svg';
import googleStoreBadge from './images/googleStoreButton.svg';

import StoreBadge from './storeBadge';

class StoreBadges extends Component {
	render() {
		return (
			<div
				className="container"
				style={{ position: 'relative', bottom: '-70vh' }}>
				<StoreBadge
					imgUrl={appStoreBadge}
					// linkTo="/home"
					onClick={() => {
						this.props.loggedInSuccessfully(true);
					}}
				/>
				{/* <StoreBadge imgUrl={googleStoreBadge} linkTo="/support" /> */}
			</div>
		);
	}
}

export default connect(null, actions)(StoreBadges);
