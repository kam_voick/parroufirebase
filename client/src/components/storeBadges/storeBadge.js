import React from 'react';
import { Link } from 'react-router-dom';

export default props => {
	return (
		<div className="row no-gutters justify-content-center">
			{/* <Link
				className="col-5 col-xs-4 col-sm-4 col-md-3 col-lg-2 my-2 h-100"
			to={props.linkTo}> */}
			<img
				className="col-5 col-xs-4 col-sm-4 col-md-3 col-lg-2 my-2 h-100 img-fluid"
				src={props.imgUrl}
				alt=""
				onClick={props.onClick}
			/>
			{/* </Link> */}
		</div>
	);
};
