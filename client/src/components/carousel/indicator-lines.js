import React from 'react';
import PropTypes from 'prop-types';

function Line(props) {
	console.log(props);
	return (
		<hr
			style={{
				border: '3px solid #d6cfcb',
				margin: '0px 5px',
				width: '100%',
				opacity: props.selected ? '1' : '0.3',
				transitionDuration: '300ms'
			}}
		/>
	);
}

export default function IndicatorLines(props) {
	const wrapperStyle = {
		display: 'flex',
		justifyContent: 'space-evenly',
		width: '30%',
		paddingBottom: '10px',
		height: '100%',
		alignItems: 'flex-end',
		margin: 'auto',
		zIndex: '100',
		position: 'relative'
	};

	if (props.total < 2) {
		// Hide dots when there is only one dot.
		return <div style={wrapperStyle} />;
	} else {
		return (
			<div style={wrapperStyle}>
				{Array.apply(null, Array(props.total)).map((x, i) => {
					return <Line key={i} selected={props.index === i} />;
				})}
			</div>
		);
	}
}

IndicatorLines.propTypes = {
	index: PropTypes.number.isRequired,
	total: PropTypes.number.isRequired
};
