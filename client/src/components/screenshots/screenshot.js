import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import { Transition } from 'react-transition-group';

export default class Screenshot extends Component {
	constructor(props) {
		super(props);

		this.state = {
			topOffset: 1000,
			bottomOffset: 0,
			hovered: false
		};

		this.calculateBottomOffset = this.calculateBottomOffset.bind(this);
		this.handleMouseHover = this.handleMouseHover.bind(this);

		window.addEventListener('resize', this.calculateBottomOffset);
	}

	componentDidUpdate(props) {
		if (this.props.scrollUp && this.state.bottomOffset === 0) {
			this.calculateBottomOffset();
		}
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.calculateBottomOffset);
	}

	calculateBottomOffset() {
		const { clientHeight } = findDOMNode(this.imgRef);

		const topOffset = clientHeight - 227 / 1776 * clientHeight;
		const bottomOffset = 214 / 1776 * clientHeight;

		this.setState({ topOffset, bottomOffset });
	}

	handleMouseHover() {
		this.setState({ hovered: !this.state.hovered });
	}

	render() {
		const defaultStyle = {
			position: 'relative',
			pointerEvents: 'all',
			transform: 'translateY(1000px)',
			transition: `transform 0.6s ease-out ${this.state.hovered
				? 0
				: this.props.delay || 0}s`
		};

		const transitionStyles = {
			entering: {
				transform: `translateY(${this.state.hovered
					? this.state.bottomOffset
					: this.state.topOffset}px)`
			},
			entered: {
				transform: `translateY(${this.state.hovered
					? this.state.bottomOffset
					: this.state.topOffset}px)`
			}
		};

		return (
			<div
				className={this.props.className}
				style={{ overflow: 'hidden', pointerEvents: 'none' }}>
				<Transition
					in={this.state.bottomOffset !== 0}
					appear={true}
					onEntered={() => {
						console.log('entered');
					}}
					timeout={0}>
					{state => (
						<div
							style={{
								...defaultStyle,
								...transitionStyles[state]
							}}>
							<img
								ref={ref => {
									this.imgRef = ref;
								}}
								onMouseEnter={this.handleMouseHover}
								onMouseLeave={this.handleMouseHover}
								onLoad={this.props.imageLoaded}
								src={this.props.url}
								alt=""
								className="img-fluid px-2"
							/>
						</div>
					)}
				</Transition>
			</div>
		);
	}
}
