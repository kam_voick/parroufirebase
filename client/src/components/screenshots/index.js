import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';

import screenshot1 from './images/pl/ip6_pl_screen1.png';
import screenshot2 from './images/pl/ip6_pl_screen4.png';
import loginScreenshot from './images/pl/ip6_pl_loginScreen.png';
import screenshot3 from './images/pl/ip6_pl_screen2.png';
import screenshot4 from './images/pl/ip6_pl_screen3.png';

import Screenshot from './screenshot';

function imagesLoaded(parentNode) {
	const imgElements = parentNode.querySelectorAll('img');
	for (const img of imgElements) {
		if (!img.complete) {
			return false;
		}
	}
	return true;
}

export default class Screenshots extends Component {
	constructor(props) {
		super(props);
		this.state = { imagesLoaded: false };

		this.handleStateChange = this.handleStateChange.bind(this);
	}

	componentDidMount() {}

	handleStateChange() {
		const screenshotsDiv = this.screenshotsDiv;
		if (imagesLoaded(screenshotsDiv) && !this.state.imagesLoaded) {
			this.setState({ imagesLoaded: true });
		}
	}

	render() {
		console.log('render');
		console.log(this.state.imagesLoaded);
		return (
			<div
				ref={ref => (this.screenshotsDiv = ref)}
				className="row h-100 no-gutters">
				<div className="row no-gutters align-items-end">
					<Screenshot
						className="col d-none d-sm-none d-md-none d-lg-block"
						url={screenshot1}
						delay="0.2"
						imageLoaded={this.handleStateChange}
						scrollUp={this.state.imagesLoaded}
					/>
					<Screenshot
						className="col d-none d-sm-none d-md-block d-lg-block"
						url={screenshot2}
						delay="0.1"
						imageLoaded={this.handleStateChange}
						scrollUp={this.state.imagesLoaded}
					/>
					<Screenshot
						className="col col-lg-3 col-md-5 d-none d-sm-none d-md-block d-lg-block"
						url={loginScreenshot}
						imageLoaded={this.handleStateChange}
						scrollUp={this.state.imagesLoaded}
					/>
					<Screenshot
						className="col d-none d-sm-none d-md-block d-lg-block"
						url={screenshot3}
						delay="0.1"
						imageLoaded={this.handleStateChange}
						scrollUp={this.state.imagesLoaded}
					/>
					<Screenshot
						className="col d-none d-sm-none d-md-none d-lg-block"
						url={screenshot4}
						delay="0.2"
						imageLoaded={this.handleStateChange}
						scrollUp={this.state.imagesLoaded}
					/>
				</div>
			</div>
		);
	}
}
